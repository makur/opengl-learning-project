#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

int main()
{
    // 把一个向量(1, 0, 0)位移(1, 1, 0)个单位
    glm::vec4 initVec(1.0f, 0.0f, 0.0f, 1.0f);
    glm::mat4 transMove = glm::mat4(1.0f);
    transMove = glm::translate(transMove, glm::vec3(1.0f, 1.0f, 0.0f));
    initVec = transMove * initVec;
    std::cout << " x: " << initVec.x << " y: " << initVec.y << " z: " << initVec.z << std::endl;

    return 0;
}
