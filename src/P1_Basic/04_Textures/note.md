
# 前言

 纹理(Texture)。纹理是一个2D图片（甚至也有1D和3D的纹理），它可以用来添加物体的细节。
 你可以想象纹理是一张绘有砖块的纸，无缝折叠贴合到你的3D的房子上，这样你的房子看起来就像有砖墙外表了。
 >除了图像以外，纹理也可以被用来储存大量的数据，这些数据可以发送到着色器上。

 **纹理映射(Map)**

 下面的图片展示了我们是如何把纹理坐标映射到三角形上的。

![Alt text](tex_coords.png)

纹理坐标看起来就像这样：

```
float texCoords[] = {
    0.0f, 0.0f, // 左下角
    1.0f, 0.0f, // 右下角
    0.5f, 1.0f // 上中
};
```

# 纹理环绕方式

纹理坐标的范围通常是从(0, 0)到(1, 1)。

OpenGL中的纹理环绕方式：

环绕方式 描述
GL_REPEAT 对纹理的默认行为。重复纹理图像。
GL_MIRRORED_REPEAT 和GL_REPEAT一样，但每次重复图片是镜像放置的。
GL_CLAMP_TO_EDGE 纹理坐标会被约束在0到1之间，超出的部分会重复纹理坐标的边缘，产生一种边缘被拉伸的效果。
GL_CLAMP_TO_BORDER 超出的坐标为用户指定的边缘颜色。

对应的纹理效果图：

![Alt text](texture_wrapping.png)

**glTexParameter*函数**

使用glTexParameter*函数对单独的一个坐标轴设置（s、t（如果是使用3D纹理那么还有一个r）它们和x、y、z是等价的）：

```
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
```

- 第一个参数指定了纹理目标；我们使用的是2D纹理，因此纹理目标是GL_TEXTURE_2D。
- 第二个参数需要我们指定设置的选项与应用的纹理轴。我们打算配置的是WRAP选项，并且指定S和T轴。
- 最后一个参数需要我们传递一个环绕方式(Wrapping)。

**glTexParameterfv函数**

只有选择GL_CLAMP_TO_BORDER模式时需要用到这个函数，同时需要指定一个边缘的颜色。

```
float borderColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };//边缘的颜色值
glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
```

# 纹理过滤

纹理坐标不依赖于分辨率(Resolution)，它可以是任意浮点值。在OpenGL中使用纹理过滤(Texture Filtering)的方式将纹理像素(Texture Pixel)映射到纹理坐标上。

最重要的两种纹理过滤方式：

## GL_NEAREST(邻近过滤，Nearest Neighbor Filtering)

邻近过滤是OpenGL默认的纹理过滤方式,当设置为GL_NEAREST的时候，OpenGL会选择中心点最接近纹理坐标的那个像素。

![Alt text](image.png)

## GL_LINEAR（线性过滤，(Bi)linear Filtering）

线性过滤会基于纹理坐标附近的纹理像素，计算出一个插值，近似出这些纹理像素之间的颜色。

一个纹理像素的中心距离纹理坐标越近，那么这个纹理像素的颜色对最终的样本颜色的贡献越大。

![Alt text](image-1.png)

## 对比

![Alt text](image-2.png)

这个是两种过滤方式的对比图，对比结果：

- 1. `GL_NEAREST`产生了颗粒状的图案，我们能够清晰看到组成纹理的像素，而`GL_LINEAR`能够产生更平滑的图案，很难看出单个的纹理像素

- 2. `GL_LINEAR`可以产生更真实的输出，但有些开发者更喜欢8-bit风格，所以他们会用`GL_NEAREST`选项。

- 3. 当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项，比如你可以在纹理被缩小的时候使用邻近过滤，被放大时使用线性过滤。

**使用glTexParameter*函数为放大和缩小指定过滤方式**

```
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
```

# 多级渐远纹理(Mipmap)

多级渐远纹理：其实就是距观察者的距离超过一定的阈值，OpenGL会使用不同的多级渐远纹理，即最适合物体的距离的那个。如果接触过LOD就会发现它俩很相似。

参考图：

![Alt text](mipmaps.png)

**glGenerateMipmaps函数**

glGenerateMipmaps函数：是在OpenGL中用于创建多级渐远纹理。

过滤方式 | 描述
GL_NEAREST_MIPMAP_NEAREST | 使用最邻近的多级渐远纹理来匹配像素大小，并使用邻近插值进行纹理采样
GL_LINEAR_MIPMAP_NEAREST | 使用最邻近的多级渐远纹理级别，并使用线性插值进行采样
GL_NEAREST_MIPMAP_LINEAR | 在两个最匹配像素大小的多级渐远纹理之间进行线性插值，使用邻近插值进行采样
GL_LINEAR_MIPMAP_LINEAR | 在两个邻近的多级渐远纹理之间使用线性插值，并使用线性插值进行采样

使用glTexParameteri函数调用：

```
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
```

>一个常见的错误是，将放大过滤的选项设置为多级渐远纹理过滤选项之一。这样没有任何效果，`因为多级渐远纹理主要是使用在纹理被缩小的情况下的：纹理放大不会使用多级渐远纹理`，为放大过滤设置多级渐远纹理的选项会产生一个GL_INVALID_ENUM错误代码。

# 加载与创建纹理

## stb_image.h库

使用[stb_image.h库](https://github.com/nothings)来加载图像，它是一个支持多种流行格式的图像加载库。stb_image.h可以在[这里下载](https://github.com/nothings/stb/blob/master/stb_image.h)。

使用stb_image.h加载图片示例：

```cpp
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

...
int width, height, nrChannels;//宽度、高度和颜色通道的个数
unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);
...
```

## 生成纹理

**glTexImage2D函数**

API[官方文档](https://learn.microsoft.com/zh-cn/windows/win32/opengl/glteximage2d)

···
void glTexImage2D(
         GLenum  target,
         GLint   level,
         GLint   internalformat,
         GLsizei width,
         GLsizei height,
         GLint   border,
         GLint   format,
         GLenum  type,
   const GLvoid  *pixels
);
···

- target：目标纹理。 必须为 GL_TEXTURE_2D。
- level：细节级别编号。 级别0是基础图像级别。 级别n是第n个mipmap缩减图像。
- internalformat：纹理中颜色分量的数量。可以为1、2、3或4或符号常量（GL_RGB、GL_RGBA、GL_RGBA16等等）。
- width：纹理图像的宽度。 对于某些整数n，必须为n + 2(border)。
- height：纹理图像的高度。 对于某些整数m，必须为2*m* + 2(border)。
- border：边框的宽度。 必须为 0 或 1。
- format：像素数据的格式。比如：GL_UNSIGNED_BYTE、GL_COLOR_INDEX、GL_ALPHA等等。
- type：像素数据的数据类型。比如：GL_UNSIGNED_BYTE、GL_BYTE、GL_BITMAP等等。
- pixels：指向内存中图像数据的指针，是真正的图像数据。

